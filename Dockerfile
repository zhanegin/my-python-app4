FROM python:3.11

WORKDIR /app
ADD . /app/
RUN pip install -r requirements.txt

EXPOSE 3000
CMD ["python", "/app/main.py"]
